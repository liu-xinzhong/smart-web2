package cn.com.smart.form.list.bean;

/**
 * 日期列表属性字段
 * @author lmq
 */
public class DateListFieldProp extends AbstractListFieldProp {

    private String dateType;

    private String dateFormat;

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }
    
}
