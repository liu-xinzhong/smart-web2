package cn.com.smart.form.list.bean;

import cn.com.smart.form.enums.FormListType;

import java.util.List;

/**
 * 表单列表属性
 * @author lmq
 *
 */
public class FormListProp {

    private String formId;

    private String userId;
    
    private String formName;
    
    private String formType;

    private String formListType = FormListType.MAIN.getValue();
    
    private List<AbstractListFieldProp> listFieldProps;

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public List<AbstractListFieldProp> getListFieldProps() {
        return listFieldProps;
    }

    public void setListFieldProps(List<AbstractListFieldProp> listFieldProps) {
        this.listFieldProps = listFieldProps;
    }

    public String getFormListType() {
        return formListType;
    }

    public void setFormListType(String formListType) {
        this.formListType = formListType;
    }
}
