package cn.com.smart.form.service;

import cn.com.smart.form.bean.entity.TFormList;

import java.util.List;

/**
 * @author 乌草坡 2019-09-02
 * @since 1.0
 */
public interface IFormListService {

    /**
     * 创建表单列表
     * @param formLists
     */
    void createList(List<TFormList> formLists);

}
