package cn.com.smart.form.bean.entity;

import cn.com.smart.bean.LogicalDeleteSupport;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 乌草坡 2019-09-15
 * @since 1.0
 */
@Entity
@Table(name = "t_form_field_mapping")
public class TFormFieldMapping extends LogicalDeleteSupport {

    private String id;

    private String formMappingId;

    private String fieldId;

    private String targetFieldId;

    private Integer sortOrder = 0;

    @Id
    @Column(name = "id", length = 50)
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "form_mapping_id", length = 50, nullable = false)
    public String getFormMappingId() {
        return formMappingId;
    }

    public void setFormMappingId(String formMappingId) {
        this.formMappingId = formMappingId;
    }

    @Column(name = "field_id", length = 50, nullable = false)
    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    @Column(name = "target_field_id", length = 50, nullable = false)
    public String getTargetFieldId() {
        return targetFieldId;
    }

    public void setTargetFieldId(String targetFieldId) {
        this.targetFieldId = targetFieldId;
    }

    @Column(name = "sort_order")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
}
